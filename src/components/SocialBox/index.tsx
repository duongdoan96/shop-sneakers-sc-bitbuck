import React from 'react';

interface SocialBoxType {
  name: string;
  number: number;
  bgColor: string;
  icon: string
}

function SocialBox(props: SocialBoxType) {
  return (
    <div className="social-box" style={{backgroundColor: `${props.bgColor}`}}>
      <i className={`fab fa-${props.icon}`}></i>
      <h6>{props.name}</h6>
      <h1>{props.number}</h1>
      <span>#AgoraStore</span>
    </div>
  )
}

export default SocialBox
