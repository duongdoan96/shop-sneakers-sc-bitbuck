import React from 'react';

interface ServiceBoxType {
  url: string;
  title: string;
  des: string
}

function ServiceBox(props: ServiceBoxType) {
  return (
    <section className="service-box">
        <img src={props.url} alt="" />
        <h3>{props.title}</h3>
        <p>{props.des}</p>
    </section>
  )
}

export default ServiceBox
