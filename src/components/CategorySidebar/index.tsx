import React, { useState } from 'react';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import { FormGroup, Input, Label } from 'reactstrap';

interface CategorySidebarTypes {
  showListCate: boolean,
  showPrice: boolean,
  name: string
}


function CategorySidebar(props) {
  const [isShowBody, setIsShowBody]  = useState(true)
  const [isShowEl, setIsShowEl] = useState(true)
  const [valueRange, setValueRange] = React.useState({
    min: 50,
    max: 5000,
  })

  const colorOption = [
    {value: 'white', color: '#ffffff', background: ''},
    {value: 'silver', color: '#e5e5e5', background: ''},
    {value: 'yellow', color: '#feb312', background: ''},
    {value: 'orange', color: '#f36b26', background: ''},
    {value: 'red', color: '#e7352b', background: ''},
    {value: 'purple', color: '#8d429f', background: ''},
    {value: 'blue', color: '#3a54d6', background: ''},
    {value: 'bluesky', color: '#23a5e8', background: ''},
    {value: 'green', color: '#5bb22c', background: ''},
    {value: 'brown', color: '#7c5e4e', background: ''},
    {value: 'black', color: '#7c5e4e', background: ''},
  ]

  let sizeOption: number[] = []

  for(let i = 4; i <= 19; ) {
    if(i < 15) {
      sizeOption.push(i)
      i += 0.5
    }

    if(i >= 15) {
      sizeOption.push(i)
      i++
    }
  }


  const handleClick = () => {
    let toggle = !isShowBody
    setIsShowBody(toggle)
  }

  const hanldeDelete = () => {
    let toggle = !isShowEl
    setIsShowEl(toggle)
  }

  return (
    <div className="sidebar__category">

      {
        isShowEl ?
        <div className="category__header">

          <div className="header__wrap"  onClick={handleClick}>
            <i className="fas fa-sort-down"></i>
            <h6>{props.name}</h6>
          </div>

          <span onClick={hanldeDelete}>x</span>
        </div> : ''
      }

      {
        isShowBody && props.showListCate ?
        <div className="category__body__list">
          <div className="category__item">
            <p>Lifetyle</p>
            <span>1</span>
          </div>
          <div className="category__item">
            <p>Running</p>
            <span>23</span>
          </div>
          <div className="category__item">
            <p>Training</p>
            <span>45</span>
          </div>
          <div className="category__item">
            <p>Training</p>
            <span>45</span>
          </div>
          <div className="category__item">
            <p>Training</p>
            <span>45</span>
          </div>
          <div className="category__item">
            <p>Training</p>
            <span>45</span>
          </div>
          <div className="category__item">
            <p>Training</p>
            <span>45</span>
          </div>
          <div className="category__item">
            <p>Training</p>
            <span>45</span>
          </div>
          <div className="category__item">
            <p>Training</p>
            <span>45</span>
          </div>
        </div> : ''
      }

      {
        isShowBody && props.showPrice ?
        <div className="category__body__price">
          <div className="category__price__value">
            <input type="text" value={`$${valueRange.min}`} />
            <input type="text" value={`$${valueRange.max}`} />
          </div>
          <InputRange
          draggableTrack
          maxValue={10000}
          minValue={0}
          onChange={(value: any) => {
            console.log(value)
            setValueRange({
              min: value.min,
              max: value.max
            })
          }}
          // onChangeComplete={value => console.log(value)}
          value={valueRange}
          />
        </div> : ''
      }

      {
        isShowBody && props.showRadio ?
        <div className="category__body__checkbox">
          <FormGroup>
            <Label for="checkbox1">Lifestyle</Label>
            <Input type="checkbox" id="checkbox1" />
          </FormGroup>
          <FormGroup>
            <Label for="checkbox2">Running</Label>
            <Input type="checkbox" id="checkbox2" />
          </FormGroup>
          <FormGroup>
            <Label for="checkbox3">Tranining</Label>
            <Input type="checkbox" id="checkbox3" />
          </FormGroup>
          <FormGroup>
            <Label for="checkbox4">Basketball</Label>
            <Input type="checkbox" id="checkbox4" />
          </FormGroup>
        </div> : ''
      }

      {
        isShowBody && props.showColor ?
        <div className="category__body__color">
          {colorOption.map((item, index) => (
            <ColorItem key={index} color={item.color} />
          ))}
          <div className="circle">
            <i className="fas fa-check"></i>
          </div>
        </div> : ''
      }

      {
        isShowBody && props.showTable ?
        <div className="category__body__table">
          {sizeOption.map((number, index) => (
            <div key={index} className="table__item">{number}</div>
          ))}
        </div> : ''
      }
    </div>
  )
}

const ColorItem = (props: {color: string}) => {
  const [show, setShow] = useState(false)

  return (
    <div
      className="color-item"
      onClick={() => {
        setShow(prevState => !prevState)
      }}
      style={{
        backgroundColor: `${props.color}`,
      }}
      >
      <i
        className="fas fa-check"
        style={{display: `${show ? 'block' : 'none'}`}}
      ></i>
    </div>
  )
}

export default CategorySidebar
