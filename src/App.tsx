import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Cartpage from './containers/CartPage/view';
import CategoryPage from './containers/CategoryPage/view';
import HomePage from './containers/Homepage/view';
import ProductDetailPage from './containers/ProductDetailPage/view';
import Footer from './layouts/MainLayout/Footer';


function App() {

  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route path="/" exact component={HomePage}/>
          <Route path="/productDetail" exact component={ProductDetailPage} />
          <Route path="/category" exact component={CategoryPage} />
          <Route path="/cart" exact component={Cartpage} />
        </Switch>
        <Footer />
      </div>
    </BrowserRouter>
  )
}

export default App
