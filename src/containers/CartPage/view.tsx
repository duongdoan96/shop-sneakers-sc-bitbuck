import Header from '../../layouts/MainLayout/Header'
import React, { Fragment } from 'react'

function Cartpage() {
  return (
    <Fragment>
      <Header isHomepage={false} />
      <div className="cart-page">
        <section className="checkout">

        </section>

        <section className="ship-info">

        </section>

        <section className="paymnet">

        </section>
      </div>
    </Fragment>
  )
}

const CartProductItem = () => {
  return (
    <div className="cart-product-item">
      <div className="img"></div>
      <div className="name">
        <h6>Jodan Galaxy</h6>
        <span>LIFESTYLE</span>
      </div>
      <div className="price"></div>
      <div className="quantity">
        <ul>
          <li><a href="">-</a></li>
          <li className="center"><a href="">2</a></li>
          <li><a href="">+</a></li>
        </ul>
      </div>
      <div></div>
    </div>
  )
}

export default Cartpage
