import Content from '../../layouts/MainLayout/Content/index';
import WrapServiceBox from '../../layouts/MainLayout/Content/WrapServiceBox/index';
import WrapProductSale from '../../layouts/MainLayout/Content/WrapProductSale/index';
import WrapProductCommom from '../../layouts/MainLayout/Content/WrapProductComom/index';
import React from 'react'
import LoadMore from '../../components/LoadMore/index';
import WrapSocial from '../../layouts/MainLayout/Content/WrapSocial/index';
import Banner from '../../components/Crousel/index';
import Header from '../../layouts/MainLayout/Header/index';

function HomePage() {
  return (
    <div className="homepage">
        <Header isHomepage />
        <Banner />
        <Content>
          <WrapServiceBox />
          <WrapProductSale />
          <WrapProductCommom isCardDirect isCommomScaleBox={false} isNormal={false} />
          <WrapProductCommom isCardDirect={false} isCommomScaleBox isNormal={false} />
          <WrapProductCommom isCardDirect={false} isCommomScaleBox={false} isNormal />
          <LoadMore />
          <WrapSocial />
        </Content>
    </div>
  )
}

export default HomePage
