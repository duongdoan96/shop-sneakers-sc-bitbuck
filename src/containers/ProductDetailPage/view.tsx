import SubBanner from '../../components/SubBanner';
import React from 'react'
import CommentBox from '../../components/CommentBox';
import WrapProductCommom from '../../layouts/MainLayout/Content/WrapProductComom/index';
import Header from '../../layouts/MainLayout/Header/index';

function ProductDetailPage() {
  return (
    <>
      <Header isHomepage={false} />
      <SubBanner />
      <div className="product-detail-page">
        <div className="wrap__info">
          <div className="product-detail__info">
            <h6>LIFESTYLE</h6>
            <h1>LeBron XIII Premium AS iD</h1>

            <div className="rating">
              <ul>
                <li><i className="fas fa-star"></i></li>
                <li><i className="fas fa-star"></i></li>
                <li><i className="fas fa-star"></i></li>
                <li><i className="fas fa-star"></i></li>
                <li><i className="fas fa-star-half-alt"></i></li>
              </ul>

              <span>23 reviews</span>
            </div>

            <div className="wrap__attr">
              <div className="wrap__attr__color">
                <h6>COLORS:</h6>
                <ul>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                </ul>
              </div>

              <div className="wrap__attr__size">
                <h6>SIZE:</h6>
                <ul>
                  <li>9.5</li>
                  <li>9</li>
                  <li>10</li>
                  <li>10.5</li>
                  <li>11</li>
                </ul>
              </div>
            </div>

              <div className="wrap__payment">
                <div className="payment__price">
                  <div className="payment__price__detail">
                    <h4>$250.00</h4>
                    <span>350.00</span>
                  </div>
                  <div className="payment__discount">-30%</div>
                </div>


                <div className="payment__quantity">
                  <ul>
                    <li><a href="">-</a></li>
                    <li className="center"><a href="">2</a></li>
                    <li><a href="">+</a></li>
                  </ul>
                  <button><i className="fas fa-cart-arrow-down"></i>Buy</button>
                </div>

              </div>

              <ul className="wrap__add">
                <li><i className="far fa-copy"></i>Compare</li>
                <li><i className="far fa-heart"></i>Add to Wish List</li>
              </ul>
          </div>

          <div className="product-detail__img">500x500</div>

          <div className="product-detail__img__describe">
            <div>60x60</div>
            <div>60x60</div>
            <div>60x60</div>
            <div>60x60</div>
            <div>60x60</div>
          </div>
        </div>
        <div className="comment__nav">
            <ul>
              <li><a href="#">REVIEWS</a> </li>
              <li><a href="#">SPECIFICATION</a> </li>
              <li><a href="#">DESCRITION</a> </li>
            </ul>
          </div>
        <div className="wrap__comment">
          <div className="comment__content">
            <div className="comment__content__user">
              <h3>All Reviews <strong>23</strong></h3>
              <CommentBox />
              <CommentBox />
              <CommentBox />
              <CommentBox />
              <div className="panagation">
                <button><i className="fas fa-arrow-circle-down"></i>Load More</button>
                <ul>
                  <li><a href="#">1</a> </li>
                  <li><a href="#">2</a> </li>
                  <li><a href="#">3</a> </li>
                  <li><a href="#">4</a> </li>
                </ul>
              </div>
            </div>

            <div className="comment__review">
              <h3>Assessment reviews</h3>
              <div className="comment__review__item">
                <ul>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star-half-alt"></i></li>
                </ul>
                <p>68%</p>
                <span>14 reviews</span>
              </div>
              <div className="comment__review__item">
                <ul>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star-half-alt"></i></li>
                </ul>
                <p>68%</p>
                <span>14 reviews</span>
              </div>
              <div className="comment__review__item">
                <ul>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star-half-alt"></i></li>
                </ul>
                <p>68%</p>
                <span>14 reviews</span>
              </div>
              <div className="comment__review__item">
                <ul>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star"></i></li>
                  <li><i className="fas fa-star-half-alt"></i></li>
                </ul>
                <p>68%</p>
                <span>14 reviews</span>
              </div>
              <button>Add Review</button>
          </div>
          </div>
        </div>

        <div className="wrap__product">
          <WrapProductCommom isCardDirect isCommomScaleBox={false} isNormal={false} />
          <WrapProductCommom isCardDirect={false} isCommomScaleBox={false} isNormal />
        </div>

      </div>
    </>
  )
}

export default ProductDetailPage
